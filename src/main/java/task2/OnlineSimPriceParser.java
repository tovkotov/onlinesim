package task2;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.javascript.SilentJavaScriptErrorListener;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/*
    Парсер цен сайта OnlineSim
        В идеальном мире надо было бы составить список всех возможных счетаний стран, и услуг,
    и парсить получившийся список в многопоточном режиме. Не факт что будет выигрыш в скорости,
    ведь админы сайта могут увидеть частые запросы с одного IP адреса в единицу времени.
        Решил задачу в 4 потока по количеству услуг.
 */

public class OnlineSimPriceParser {
    private final String TITLE = "Перечень цен на услуги по странам с сайта OnlineSim";

    private static final int TIMEOUT_MILLIS = 15000;
    private String pageURL = "https://onlinesim.ru/price-list";
    private String START_SEARCH_ID = "7";
    private List<Country> allPrices;

    public void savePriceToFile(String fileName) throws IOException {
        Files.deleteIfExists(Paths.get(fileName));
        Files.writeString(Paths.get(fileName), TITLE.concat("\n"));
        Files.writeString(Paths.get(fileName), getJsonString());
    }

    public void printPriceToTerminal() throws JsonProcessingException {
        System.out.println(getJsonString());
    }

    private String getJsonString() throws JsonProcessingException {
        if (this.allPrices == null)
            this.allPrices = parsePrice();
        return new ObjectMapper().writeValueAsString(allPrices);
    }

    private List<Country> parsePrice() {
        List<Country> allPrices = new ArrayList<>();
        try {
            ConcurrentLinkedQueue<String> queueServices = new ConcurrentLinkedQueue(getServicesList());

            Callable<List<Country>> callable = () -> {
                String serviceType = queueServices.poll();
                System.out.println("OnlineSimParser: start thread: " + serviceType);
                Document doc = renderPage(START_SEARCH_ID, serviceType);
                List<Country> countries = getCountries(doc);
                if (countries.size() == 0)
                    countries.add(new Country("7", "Россия"));

                int count = 0;
                for (Country country : countries) {
                    Document page = renderPage(country.getId(), serviceType);
                    country.setPrices(parsePrice(serviceType, page));
                    System.out.println("OnlineSimParser: добавлена страна: " + country.getCountryName() +
                            "\n\t serviceType: " + serviceType +
                            "\n\t в прайсе: " + country.getPrices().size() + " позиций " +
                            "\n\t обработано: " + ++count + " из " + countries.size());
                }
                return countries;
            };

            ExecutorService executor = Executors.newFixedThreadPool(getServicesList().size());
            List<Future<List<Country>>> list = new ArrayList<>();
            for (int i = 0; i < getServicesList().size(); i++) {
                Future<List<Country>> future = executor.submit(callable);
                list.add(future);
            }
            for (Future<List<Country>> fut : list) {
                try {
                    allPrices.addAll(fut.get());
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
            executor.shutdown();
        } catch (IOException | IllegalArgumentException e) {
            e.printStackTrace();
        }
        return allPrices;
    }

    private List<String> getServicesList() throws IOException {
        List<String> services = new ArrayList<>();
        Document document = Jsoup.connect(pageURL)
                .maxBodySize(Integer.MAX_VALUE)
                .get();
        Elements serviceElements = document
                .select("#index > div" +
                        " > div.page-content" +
                        " > div.container.no-padding" +
                        " > div > div.col-md-12.col-xs-12.types.no-padding" +
                        " > div.col-md-3.col-xs-6.no-padding");
        for (Element element : serviceElements) {
            String str = element.getElementsByTag("a").toString();
            String[] list = str.split(" ");
            for (int i = 0; i < list.length; i++) {
                if (list[i].equals("==="))
                    services.add(list[i + 1].replaceAll("'", ""));
            }
        }
        return services;
    }

    private List<Country> getCountries(Document doc) throws IOException {
        List<Country> countryList = new ArrayList<>();
        Elements countryElements = getCountryElements(doc);
        for (Element el : countryElements) {
            countryList.add(
                    new Country(el
                            .getElementsByTag("a")
                            .attr("id")
                            .replaceAll("country-", ""),
                            el.text()));
        }
        System.out.println("OnlineSimParser: получен список стран, размер списка: " + countryList.size());
        return countryList;
    }

    private Elements getCountryElements(Document doc) {
        return doc.select("#index > div " +
                "> div.page-content " +
                "> div.container.no-padding > " +
                "> div.col-md-12.no-padding " +
                "> div.col-md-12.col-xs-12.no-padding " +
                "> div.col-md-2.col-xs-6.country-block.no-padding");
    }

    private List<Price> parsePrice(String serviceType, Document doc) {
        List<Price> prices = new ArrayList<>();
        Elements elements = doc.select("#index > div " +
                        "> div.page-content " +
                        "> div.container.no-padding " +
                        "> div > div:nth-child(3) " +
                        "> div > div")
                .select("div.col-md-2.col-xs-6.country-block.no-padding");
        if (elements.size() == 0) // в режиме forward только одна страна и генерируется другая страница
            elements = doc.select("#index > div " +
                            "> div.page-content " +
                            "> div.container.no-padding " +
                            "> div > div:nth-child(2) " +
                            "> div > div")
                    .select("div.col-md-2.col-xs-6.country-block.no-padding");
        for (Element el : elements) {
            prices.add(new Price(
                    serviceType,
                    el.getElementsByTag("span").get(0).text(),
                    el.getElementsByTag("span").get(1).text()
            ));
        }
        return prices;
    }

    private Document renderPage(String countryId, String serviceType) throws IOException, InterruptedException {
        Thread.sleep((long) (Math.random() * 1000));
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
        java.util.logging.Logger.getLogger("org.apache.http").setLevel(java.util.logging.Level.OFF);

        WebClient webClient = new WebClient(BrowserVersion.CHROME);
        webClient.getOptions().setJavaScriptEnabled(true);
        webClient.getOptions().setCssEnabled(false);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.setCssErrorHandler(new SilentCssErrorHandler());
        webClient.setJavaScriptErrorListener(new SilentJavaScriptErrorListener());
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);

        System.out.println("OnlineSimParser: запрашиваю адрес: " + pageURL + "?country=" + countryId + "&type=" + serviceType);
        URL url = new URL(pageURL + "?country=" + countryId + "&type=" + serviceType);
        WebRequest requestSettings = new WebRequest(url, HttpMethod.GET);
        HtmlPage page = webClient.getPage(requestSettings);
        synchronized (page) {
            try {
                page.wait(TIMEOUT_MILLIS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return Jsoup.parse(page.asXml());
    }
}
