package task2;

public class Price {
    private String serviceType;
    private String priceName;
    private String amount;

    public Price(String serviceType, String priceName, String amount) {
        this.serviceType = serviceType;
        this.priceName = priceName;
        this.amount = amount;
    }

    public String getServiceType() {
        return serviceType;
    }

    public String getPriceName() {
        return priceName;
    }

    public String getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "Price{" +
                "serviceType='" + serviceType + '\'' +
                ", priceName='" + priceName + '\'' +
                ", amount='" + amount + '\'' +
                '}';
    }
}
