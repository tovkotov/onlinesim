import task1.OnlineSimPhoneExtractor;
import task2.OnlineSimPriceParser;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        // task1();
        task2();
    }
    private static void task1() throws IOException {
        OnlineSimPhoneExtractor onlineSimService = new OnlineSimPhoneExtractor();
        onlineSimService.saveToFile("src/main/phones.txt");
        onlineSimService.printToConsole();
    }

    private static void task2() throws IOException {
        OnlineSimPriceParser parser = new OnlineSimPriceParser();
        parser.printPriceToTerminal();
        parser.savePriceToFile("src/main/prices.json");
    }
}
