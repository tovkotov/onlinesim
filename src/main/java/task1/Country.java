package task1;

import java.util.List;

public class Country {
    private Long id;
    private String countryName;
    private List<String> phones;

    public Country(Long id, String countryName, List<String> phones) {
        this.id = id;
        this.countryName = countryName;
        this.phones = phones;
    }

    public Long getId() {
        return id;
    }

    public List<String> getPhones() {
        return List.copyOf(phones);
    }

    public String getCountryName() {
        return countryName;
    }

    @Override
    public String toString() {
        return "task1.Country{" +
                "id=" + id +
                ", countryName='" + countryName + '\'' +
                ", phones=" + phones +
                '}';
    }
}
