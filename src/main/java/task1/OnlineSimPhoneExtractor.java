package task1;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OnlineSimPhoneExtractor {
    private final String TITLE = "Список доступных номеров OnlineSim";
    private List<Country> countries;

    public void saveToFile(String fileName) throws IOException {
        Files.deleteIfExists(Paths.get(fileName));
        Files.writeString(Paths.get(fileName), TITLE.concat("\n"));
        try {
            for(Country country : getCountries()){
                String countryName = country.getCountryName();
                Files.writeString(Paths.get(fileName), countryName.concat(":\n"), StandardOpenOption.APPEND);
                for (String phone : country.getPhones()){
                    Files.writeString(Paths.get(fileName), "\t".concat(phone).concat("\n"), StandardOpenOption.APPEND);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void printToConsole() throws IOException {
        System.out.println(TITLE);
        for(Country country : getCountries()){
            System.out.println(country.getCountryName().concat(":"));
            for (String phone : country.getPhones()){
                System.out.println("\t".concat(phone));
            }
        }
    }

    private List<Country> getCountries() throws IOException {
        if (this.countries == null) init();
        return List.copyOf(countries);
    }

    private void init() throws IOException {
        this.countries = countryListResponse();
    }

    private List<Country> countryListResponse() throws IOException {
        List<Country> countries = new ArrayList<>();
        URL url = new URL("https://onlinesim.ru/api/getFreeCountryList");
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8))) {
            String jString = reader.lines().collect(Collectors.joining(System.lineSeparator()));
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(jString);
            JSONArray jsonArray = (JSONArray) obj.get("countries");
            for (Object item : jsonArray) {
                JSONObject object = (JSONObject) parser.parse(item.toString());
                String countryName = (String) object.get("country_text");
                Long countryId = (Long) object.get("country");
                countries.add(new Country(countryId, countryName, phonesResponseByCountryId(countryId)));
            }
        } catch (Exception e) {
            connection.disconnect();
            e.printStackTrace();
        }
        connection.disconnect();
        return countries;
    }

    private List<String> phonesResponseByCountryId(Long countryId) throws IOException {
        List<String> phones = new ArrayList<>();
        URL url = new URL("https://onlinesim.ru/api/getFreePhoneList?country=" + countryId);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8))) {
            String jString = reader.lines().collect(Collectors.joining(System.lineSeparator()));
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(jString);
            JSONArray jsonArray = (JSONArray) obj.get("numbers");
            for (Object item : jsonArray) {
                JSONObject object = (JSONObject) parser.parse(item.toString());
                String country_num = (String) object.get("number");
                phones.add(country_num);
            }
        } catch (Exception e) {
            connection.disconnect();
            e.printStackTrace();
        }
        connection.disconnect();
        return phones;
    }

}
